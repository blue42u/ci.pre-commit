# Pre-commit for GitLab CI

*For the official pre-commit CI bot for GitHub, see [pre-commit.ci].*

[pre-commit] is a handy tool for managing Git pre-commit hooks, and has support for many formatters, linters, etc. across a wide variety of software projects.

`ci.pre-commit` a GitLab CI component for running `pre-commit` hooks in CI!

## Features

- Zero configuration -- just add to your `.gitlab-ci.yml` and it just works!
- Caches hook installations automatically!
- Produces a Git patch if any `pre-commit` hooks make changes -- compatible with `git apply` and `git am`!

## Usage

Add the following to your `.gitlab-ci.yml`:

```yaml
include:
- component: gitlab.com/blue42u/ci.pre-commit/lite@0.2.0
```

Configuration examples are listed in [`example.gitlab-ci.yml`](/example.gitlab-ci.yaml).

## Hook language support

pre-commit supports a [wide variety of languages](https://pre-commit.com/#supported-languages) that hooks may be written in.
This GitLab CI component supports some but not all languages, with support growing over time.
The table below lists the current support status of all languages supported by `pre-commit`:

|                                  Hook `language:` code | Supported |
| -----------------------------------------------------: | :-------: |
|               [`conda`](https://pre-commit.com/#conda) |     ❌     |
|         [`coursier`](https://pre-commit.com/#coursier) |     ❌     |
|                 [`dart`](https://pre-commit.com/#dart) |     ❌     |
|             [`docker`](https://pre-commit.com/#docker) |     ❌     |
| [`docker_image`](https://pre-commit.com/#docker_image) |     ❌     |
|             [`dotnet`](https://pre-commit.com/#dotnet) |     ❌     |
|                 [`fail`](https://pre-commit.com/#fail) |     ✅     |
|             [`golang`](https://pre-commit.com/#golang) |     ✅     |
|           [`haskell`](https://pre-commit.com/#haskell) |     ❌     |
|                   [`lua`](https://pre-commit.com/#lua) |     ❌     |
|                 [`node`](https://pre-commit.com/#node) |     ✅     |
|                 [`perl`](https://pre-commit.com/#perl) |     ❌     |
|             [`python`](https://pre-commit.com/#python) |   ✅[^1]   |
|   [`python_venv`](https://pre-commit.com/#python_venv) |   ✅[^1]   |
|                       [`r`](https://pre-commit.com/#r) |     ❌     |
|                 [`ruby`](https://pre-commit.com/#ruby) |     ❌     |
|                 [`rust`](https://pre-commit.com/#rust) |     ✅     |
|               [`swift`](https://pre-commit.com/#swift) |     ❌     |
|             [`pygrep`](https://pre-commit.com/#pygrep) |     ✅     |
|             [`script`](https://pre-commit.com/#script) |   ❌[^2]   |
|             [`system`](https://pre-commit.com/#system) |   ❌[^2]   |

## Comparison with [pre-commit.ci]

TODO

## Roadmap

This project is just getting off the ground. Expect issues, frequent breaking changes, and unoptimized code.

## License

This repository follows the [REUSE specification](https://reuse.software/).

All code is under the permissive MIT license. All examples are in the public domain.

[^1]: Only Python 3.11 is supported. If any hooks require C extensions they must have binary wheels compatible with `manylinux_2_38_x86_64`.

[^2]: Hooks using the `script` and `system` languages usually need special dependencies that will likely not be available in the helper image.

[pre-commit]: https://pre-commit.com/
[pre-commit.ci]: https://pre-commit.ci/
