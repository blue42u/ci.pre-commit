# SPDX-FileCopyrightText: 2023-2024 Jonathon Anderson <anderson.jonathonm@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

default_language_version:
  python: python3
default_install_hook_types: [pre-commit, commit-msg]
default_stages: [pre-commit]

# NB: The hooks in this file are listed in a particular order to reduce the number of times
# pre-commit has to run before getting an all-clear.

ci:
  skip:
  - copyright

repos:
# ------------------------------------------------------------------------------------
#  Automation hooks:  Hooks that can alter the semantics of files
# ------------------------------------------------------------------------------------

# ------------------------------------------------------------------------------------
#  Formatting hooks:  Hooks that alter the syntax but not semantics of files
# ------------------------------------------------------------------------------------

- repo: https://github.com/executablebooks/mdformat
  rev: '0.7.17'
  hooks:
  # Apply consistent formatting to Markdown documents
  - id: mdformat
    additional_dependencies:
    - mdformat-footnote
    - mdformat-tables

- repo: https://github.com/sbrunner/hooks
  rev: 0.7.0
  hooks:
  # Update copyright headers with the year logged in the Git history
  - id: copyright-required
    alias: copyright
    args: [--config, '.copyright.yaml']
    exclude: |
      (?x)^(
        # These are the actual license files themselves
        | LICENSES/[^/]+\.txt | LICENSE
        # These files use separate .license files
        | README\.md
      )$

- repo: https://github.com/pre-commit/pre-commit-hooks
  rev: v4.5.0
  hooks:
  # Remove trailing whitespace
  - id: trailing-whitespace
  # All files must end in a single newline (or be perfectly empty)
  - id: end-of-file-fixer
  # Remove the UTF8 BOM from the start of any files
  - id: fix-byte-order-marker
  # Ensure files have consistent endings. (This operates in the worktree, Git also normalizes the index)
  - id: mixed-line-ending


# ------------------------------------------------------------------------------------
#  Linting hooks:  Hooks that do not alter files but checks that they satisfy various conditions
# ------------------------------------------------------------------------------------

- repo: meta
  hooks:
  # Check that hooks listed actually do something
  - id: check-hooks-apply
  # Check that any excludes do indeed exclude something
  - id: check-useless-excludes

- repo: https://github.com/fsfe/reuse-tool
  rev: v3.0.1
  hooks:
  # Ensure sources comply with the REUSE specification for license headers
  - id: reuse

- repo: https://github.com/pre-commit/pre-commit-hooks
  rev: v4.5.0
  hooks:
  # Ensure all executable scripts have a shebang
  # - id: check-executables-have-shebangs
  # Ensure symlinks always point to something
  # - id: check-symlinks
  # Warn if symlinks are ever accidentally destroyed
  - id: destroyed-symlinks
  # Ensure conflict markers are never committed anywhere
  - id: check-merge-conflict
  # Ensure files do not differ only in case (problematic on some filesystems)
  - id: check-case-conflict
  # Reminder to always work in a branch separate from main
  - id: no-commit-to-branch
    args: [--branch, main]

- repo: https://github.com/editorconfig-checker/editorconfig-checker.python
  rev: '2.7.3'
  hooks:
  # Run a separate checker to ensure the .editorconfig rules are being followed
  - id: editorconfig-checker
    alias: ec
    args: [-disable-indent-size, -disable-max-line-length]

# - repo: https://github.com/shellcheck-py/shellcheck-py
#   rev: v0.9.0.5
#   hooks:
#   # Find common errors in shell scripts using shellcheck
#   - id: shellcheck

- repo: https://github.com/python-jsonschema/check-jsonschema
  rev: '0.27.4'
  hooks:
  # Validate the GitLab CI scripts against the schema. Doesn't catch everything but helps.
  - id: check-gitlab-ci
    files: '.*\.gitlab-ci\.yml$'

- repo: https://github.com/codespell-project/codespell
  rev: v2.2.6
  hooks:
  # Identify common spelling mistakes in code and comments
  - id: codespell
    stages: [pre-commit, commit-msg]
    args: ['--config', '.codespellrc']

- repo: https://github.com/Yelp/detect-secrets
  rev: v1.4.0
  hooks:
  # Scan for secrets that should never appear in the repo itself
  - id: detect-secrets
